# FellScout

This is a continuation of the 'felltrack scripts'; some tools that we use at the Chiltern 20 and Southern 50 
events around the FellTrack system:

https://github.com/BigRedS/felltrack

https://felltrack.com

It's called 'FellScout' because we're Scouts and that sounded less creepy than ScoutTrack.
