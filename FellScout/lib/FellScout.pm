package FellScout;

use Dancer2;
use JSON qw/decode_json encode_json/;
use Data::Dumper;

our $VERSION = '0.1';

hook 'before' => sub{
  my $cmd = config->{commands}->{get_data};
  info(" Running command '$cmd'");
  my $json = `$cmd`;
  my $progress_data = decode_json( $json );
  var entrants_progress => $progress_data->{entrants};
  var teams_progress => $progress_data->{teams};
  var route_checkpoints => get_route_checkpoints_hash();
};

get '/teams' => sub {
  template 'teams';  
};

get '/api/entrants' => sub {
  my $entrants_progress = vars->{entrants_progress};
  encode_json($entrants_progress);
};

get '/api/teams' => sub {
  my $entrants_progress = vars->{entrants_progress};
  my $teams_progress = vars->{teams_progress};
  my $routes_cps = vars->{route_checkpoints};
  my $checkpoint_times = get_checkpoint_times( $entrants_progress );

  $teams_progress = add_checkpoint_expected_at_times( $teams_progress, $checkpoint_times);
  encode_json($teams_progress);
};

get '/api/teams/table' => sub {
  my $entrants_progress = vars->{entrants_progress};
  my $teams_progress = vars->{teams_progress};
  my $routes_cps = vars->{route_checkpoints};
  my $checkpoint_times = get_checkpoint_times( $entrants_progress );

  $teams_progress = add_checkpoint_expected_at_times( $teams_progress, $checkpoint_times);
  return encode_json(create_team_summary_table($teams_progress));
};

true;

# # #
# #
#

# Headings:
# Team Number | Team Name | Route | Last checkpoint | Next chekpoint | time expected at next |  How late they are
sub create_team_summary_table{
  my $teams_progress = shift;
  my @table;
  foreach my $team_number (sort(keys(%{$teams_progress}))){
    my $t = $teams_progress->{$team_number};
    my $expected_at_next = $t->{checkpoints}->{ $t->{next_checkpoint} }->{expected_time} if $t->{checkpoints}->{ $t->{next_checkpoint} }->{expected_time};
    my $checkin_at_last = $t->{checkpoints}->{ $t->{last_checkpoint} }->{arrived_time} if $t->{checkpoints}->{ $t->{last_checkpoint} }->{arrived_time};
    my @row = ($team_number, $t->{team_name}, $t->{route}, $t->{last_checkpoint}, $t->{next_checkpoint}, $expected_at_next, time() - $expected_at_next);
    info(" [create_team_summary_table] Team $team_number; next: $t->{next_checkpoint}; last: $t->{last_checkpoint}");
    push(@table, \@row)
  }
  return \@table;
}


# Given three arguments - a teams_progress hash as returned from the progress-to-csv tool, a 
# routes_checkpoints hash as returned by the get_routes_checkpoints_hash() function and a 
# checkpoint_times hash as returned by get_checkpoint_times - will return that teams_progress
# hash with the addition of expected_time hashes on every checkpoint for which it can be
# calculated, based on the 90th percentile of the times taken betwen checkpoints.
#
sub add_checkpoint_expected_at_times {
  my $teams_progress = shift;
  my $checkpoint_times = shift;

  my $routes_cps = vars->{route_checkpoints};
  foreach my $team_number (keys(%{$teams_progress})){
    my $route_name = $teams_progress->{$team_number}->{route};
    #debug("[add_checkpoint_expected_at_times] Adding checkpoint times for team '$team_number' on route '$route_name'");
    my @route_cps = @{ $routes_cps->{$route_name} };
    #debug("[add_checkpoint_expected_at_times] Team $team_number checkpoints: ".join(" ", @route_cps));

    $teams_progress->{$team_number}->{route_checkpoints} = \@route_cps;
     
    for (my $cp=1; $cp<=$#route_cps; $cp++){
      my $this_cp = $route_cps[$cp];
      my $prev_cp = $route_cps[$cp -1 ];
      my $leg = "$prev_cp $this_cp";
      my $time_from = undef;

      if(!$teams_progress->{$team_number}->{checkpoints}->{$prev_cp}->{arrived_time} and !$teams_progress->{$team_number}->{checkpoints}->{$prev_cp}->{expected_time}){
        error("Missing expected_time and arrived_time for team $team_number at previous checkpoint $prev_cp");
        next;
      }

      #debug("[add_checkpoint_expected_at_times] Team_number: $team_number, prev_cp: $prev_cp");
      if($teams_progress->{$team_number}->{checkpoints}->{$prev_cp}->{arrived_time}){
        $time_from = $teams_progress->{$team_number}->{checkpoints}->{$prev_cp}->{arrived_time};
      }else{
        $time_from = $teams_progress->{$team_number}->{checkpoints}->{$prev_cp}->{expected_time};
      }

      my $diff = $checkpoint_times->{$leg}->{ninetieth_percentile};
      if (!$diff or $diff == 0){
        error("Got zero diff for leg '$leg'");
        next;
      }

      #debug("[add_checkpoint_expected_at_times] Expected time between checkpoints '$leg' is $diff seconds");

      my $expected = $time_from + $diff;

      $teams_progress->{$team_number}->{checkpoints}->{$this_cp}->{expected_time} = $expected;
      #debug("[add_checkpoint_expected_at_times] Team $team_number is expected at $this_cp at ".localtime($expected));
    }

    for (my $i=1; $i<=$#route_cps; $i++){
      my $cp = $route_cps[$i];
      next if ($teams_progress->{$team_number}->{checkpoints}->{$cp}->{arrived_time});
      $teams_progress->{$team_number}->{next_checkpoint} = $cp;
      $teams_progress->{$team_number}->{last_checkpoint} = $route_cps[$i - 1];
      last;
    }
  }

  return $teams_progress;
}


# Given an entrants_progress hash, iterates through all the entrants and tots up how long each took
# to do each leg, then calculates the 90th percentile of those figures (using get_percentile() and 
# creates a hash checkpoint_times, with keys that are the leg (from-cp and to-cp separated by a 
# single space)
# 
sub get_checkpoint_times {
  my $entrants = shift;
  my $checkpoint_times = {};

  my $route_cps = vars->{route_checkpoints};

  # Foreach entrant...
  foreach my $code (keys(%{$entrants})){
    my $entrant = $entrants->{$code};
    # An array of numbers, the list of checkpoints this entrant should check-in at
    my @route_cps = @{$route_cps->{ $entrant->{route} } };
    
    for (my $cp=1; $cp<=$#route_cps; $cp++){
      my $this_cp = $route_cps[$cp];
      my $prev_cp = $route_cps[$cp -1 ];
      my $leg = "$prev_cp $this_cp";

      if(!$entrant->{checkpoints}->{$this_cp}->{arrived_time}){
        #error("[get_checkpoint_times] Missing this_cp arrived_time for cp $this_cp on leg $leg");
        next;
      }
      if(!$entrant->{checkpoints}->{$prev_cp}->{arrived_time}){
        #error("[get_checkpoint_times] Missing prev_cp arrived time for cp $prev_cp (this is $this_cp) on leg $leg");
        next;
      }

      my $diff = $entrant->{checkpoints}->{$this_cp}->{arrived_time} - $entrant->{checkpoints}->{$prev_cp}->{arrived_time};
      #debug("[get_checkpoint_times] Entrant $code; cp: $this_cp; this_arr: ".$entrant->{checkpoints}->{ $this_cp }->{arrived_time}."; prev arr: ". $entrant->{checkpoints}->{$prev_cp}->{arrived_time});

      if($diff <= 0 ){
        error("Bad diff for $entrant->{code} between $prev_cp and $this_cp: $diff ($entrant->{checkpoints}->{ $this_cp }->{arrived_time} - $entrant->{checkpoints}->{$prev_cp}->{arrived_time})");
      }else{
        push(@{$checkpoint_times->{$leg}->{diffs}}, $diff);
      }
    }

    foreach my $leg (keys(%{$checkpoint_times})){
      $checkpoint_times->{$leg}->{ninetieth_percentile} = get_percentile(90, $checkpoint_times->{$leg}->{diffs});
      #debug("[get_checkpoint_times] Leg '$leg', Percentile: ".$checkpoint_times->{$leg}->{ninetieth_percentile})
    }
  }
  #foreach my $leg (sort(keys(%{$checkpoint_times}))){
  #  my @numbers = @{$checkpoint_times->{$leg}->{diffs}};
  #  debug("[get_checkpoint_times] Leg $leg; percentile: $checkpoint_times->{$leg}->{ninetieth_percentile}; sample: ".$#numbers);
  #}
  return $checkpoint_times;
}

sub get_percentile{
  my $percentile = shift;
  my $n = shift;
  my @numbers = sort(@{$n});
  my $index = int(($percentile/100) * $#numbers - 1);
  return $numbers[$index];
}


# Convert from the
#   $routes->{route_name} = "1 2 3 4 5"
# that comes from the config file to
#   $routes->{route_name} = (1, 2, 3, 4, 5);
# that's eaier to iterate over
# 
sub get_route_checkpoints_hash{
  my $route_cps = {};
  my $route_config = config->{routes};
  foreach my $route_name (keys(%{$route_config})){
    @{$route_cps->{$route_name}} = split(m/\s+/, $route_config->{$route_name}->{checkpoints});
    debug("[get_route_checkpoints_hash] Found route '$route_name': ".join(', ', @{$route_cps->{$route_name}}));
  }
  return $route_cps;
}